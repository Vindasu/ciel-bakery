import React from 'react';
import { Typography, Container, Box } from '@mui/material';

const AboutPage = () => {
    return (
        <Container maxWidth="sm">
            <Box sx={{ my: 4 }}>
                <Typography variant="h4" component="h1" gutterBottom sx={{ textAlign: 'center' }}>
                    About Us
                </Typography>
                <Typography variant="body1" gutterBottom>
                    Welcome to Ciel Bakery, where every dessert is a crafted masterpiece, meticulously prepared to deliver an unforgettable experience. From the first bite to the last, we prioritize quality, ensuring that each moment spent indulging in our treats is pure bliss. Join us in savoring life's sweetest moments, one delectable bite at a time.
                </Typography>
            </Box>
        </Container>
    );
};

export default AboutPage;