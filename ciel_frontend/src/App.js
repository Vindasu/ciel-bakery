import React from 'react';
import { Box, CssBaseline } from '@mui/material';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import HomePage from './HomePage';
import AboutPage from './AboutPage';
import StorePage from './StorePage';
import RecipesPage from './RecipesPage';
import ContactPage from './ContactPage';
import Nav from './Nav';
import Background from './Background';

function App() {
  return (
    <Box sx={{ minHeight: '100vh', position: 'relative' }}>
      <Box sx={{ position: 'absolute', zIndex: 1, width: '100%', height: '100%' }}>
        <Background />
      </Box>
      <CssBaseline />
      <Router>
        <Box sx={{ position: 'relative', zIndex: 2, background: 'transparent', boxShadow: 'none', border: 'none' }}>
          <Nav />
        </Box>
        <Box sx={{ position: 'relative', zIndex: 3, pointerEvents: 'none' }}>
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/about" element={<AboutPage />} />
            <Route path="/store" element={<StorePage sx={{ pointerEvents: 'auto' }} />} />
            <Route path="/recipes" element={<RecipesPage sx={{ pointerEvents: 'auto' }} />} />
            <Route path="/contact" element={<ContactPage sx={{ pointerEvents: 'auto' }} />} />
          </Routes>
        </Box>
      </Router>
    </Box>
  );
}

export default App;