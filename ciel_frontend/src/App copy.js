// App.js
import React, { useState } from 'react';
import { Box, CssBaseline } from '@mui/material';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import HomePage from './HomePage';
import AboutPage from './AboutPage';
import StorePage from './StorePage';
import RecipesPage from './RecipesPage';
import ContactPage from './ContactPage';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Nav from './Nav';
import Background from './Background';

function App() {
  const [darkMode, setDarkMode] = useState(false);

  const theme = createTheme({
    palette: {
      mode: darkMode ? 'dark' : 'light',
      background: {
        default: darkMode ? '#0a192f' : '#b3cde0', // dark blue for dark mode, light blue for light mode
      },
      primary: {
        main: darkMode ? '#0a192f' : '#b3cde0', // dark blue for dark mode, light blue for light mode
      },
    },
    typography: {
      fontFamily: 'Roboto Flex, sans-serif',
    },
  });

  const handleThemeChange = () => {
    setDarkMode(!darkMode);
  };

  return (
    <ThemeProvider theme={theme}>
      <Box sx={{ minHeight: '100vh', position: 'relative' }}>
        <Box sx={{ position: 'absolute', width: '100%', height: '100%', backgroundColor: theme.palette.background.default }} />
        <Box sx={{ position: 'absolute', zIndex: 1 }}>
          <Background />
        </Box>
        <CssBaseline />
        <Router>
          <Box sx={{ position: 'relative', zIndex: 2 }}>
            <Nav darkMode={darkMode} handleThemeChange={handleThemeChange} />
            <Routes>
              <Route path="/" element={<HomePage />} />
              <Route path="/about" element={<AboutPage />} />
              <Route path="/store" element={<StorePage />} />
              <Route path="/recipes" element={<RecipesPage />} />
              <Route path="/contact" element={<ContactPage />} />
            </Routes>
          </Box>
        </Router>
      </Box>
    </ThemeProvider>
  );
}

export default App;