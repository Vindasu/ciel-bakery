import React, { useRef, useEffect, useState } from 'react';
import { Box } from '@mui/material';

const Background = () => {
    const canvasRef = useRef(null);
    const [clouds, setClouds] = useState([]);
    const cloudsRef = useRef(clouds);
    const [cloudImgs, setCloudImgs] = useState([]);
    const animationFrameId = useRef(null);
    const poofImg = useRef(null);
    const [poofs, setPoofs] = useState([]);

    const handleCanvasClick = (event) => {
        const rect = canvasRef.current.getBoundingClientRect();
        const x = event.clientX - rect.left;
        const y = event.clientY - rect.top;

        setClouds(clouds.filter(cloud => {
            const cloudStartX = cloud.x;
            const cloudEndX = cloud.x + cloud.img.width;
            const cloudStartY = cloud.y;
            const cloudEndY = cloud.y + cloud.img.height;

            const isClicked = x >= cloudStartX && x <= cloudEndX && y >= cloudStartY && y <= cloudEndY;

            if (isClicked) {
                setPoofs(prevPoofs => [...prevPoofs, {
                    x: cloud.x,
                    y: cloud.y,
                    width: cloud.img.width,
                    height: cloud.img.height,
                    frameIndex: 0,
                    frameCount: 0
                }]);
            }

            return !isClicked;
        }));
    };

    useEffect(() => {
        const loadImages = async () => {
            const imgUrls = Array.from({ length: 14 }, (_, i) => `/images/Cloud_${i + 1}.png`);
            const loadedImages = await Promise.all(imgUrls.map(url => {
                return new Promise((resolve, reject) => {
                    const img = new Image();
                    img.src = url;
                    img.onload = () => resolve(img);
                    img.onerror = reject;
                });
            }));
            setCloudImgs(loadedImages);
        };
        loadImages();
    }, []);

    useEffect(() => {
        const img = new Image();
        img.src = '/images/Poof_Sheet.png'; // Poof sprite sheet
        img.onload = () => {
            poofImg.current = img;
        };
        img.onerror = (error) => {
            console.error('Failed to load poof image', error);
        };
    }, []);

    useEffect(() => {
        cloudsRef.current = clouds;
    }, [clouds]);

    useEffect(() => {
        const canvas = canvasRef.current;
        if (!canvas || cloudImgs.length === 0) return;
        const context = canvas.getContext('2d');

        let lastRenderTime = performance.now(); // Initialize lastRenderTime

        const gameLoop = () => {
            const now = performance.now();
            const deltaT = (now - lastRenderTime) / 1000.0; // Get the time difference in seconds
            lastRenderTime = now; // Update lastRenderTime for the next frame

            context.clearRect(0, 0, canvas.width, canvas.height);

            clouds.forEach(cloud => {
                if (cloud.visible) {
                    context.drawImage(cloud.img, cloud.x, cloud.y, cloud.img.width, cloud.img.height);

                    cloud.x = (cloud.x + cloud.speed) % (canvas.width + cloud.img.width);
                }
            });

            poofs.forEach((poof, index) => {
                if (poof.frameIndex < 9) { // Adjusted for 9 frames
                    const frameX = (poof.frameIndex % 3) * (poofImg.current.width / 3);
                    const frameY = Math.floor(poof.frameIndex / 3) * (poofImg.current.height / 3);
                    context.drawImage(
                        poofImg.current,
                        frameX,
                        frameY,
                        poofImg.current.width / 3,
                        poofImg.current.height / 3,
                        poof.x,
                        poof.y,
                        poof.width, // Use the poof's width
                        poof.height // Use the poof's height
                    );
                    poof.frameCount++;
                    if (poof.frameCount % 8 === 0) {
                        poof.frameIndex++;
                    }
                } else {
                    poofs.splice(index, 1);
                }
            });

            animationFrameId.current = requestAnimationFrame(gameLoop);
        };

        console.log('Starting game loop'); // Log when the game loop starts
        gameLoop();

        return () => {
            cancelAnimationFrame(animationFrameId.current);
        };
    }, [clouds, cloudImgs]);

    useEffect(() => {
        const spawnCloud = () => {
            if (!canvasRef.current || cloudsRef.current.length >= 10 || cloudImgs.length === 0) return;
            console.log('Spawning cloud'); // Log when a cloud is spawned
            const img = cloudImgs[Math.floor(Math.random() * cloudImgs.length)];
            const newCloud = {
                x: -img.width - 250,
                y: Math.random() * (canvasRef.current.height - img.height),
                speed: Math.random() * 0.25 + 0.1,
                visible: true,
                img
            };
            setClouds(prevClouds => [...prevClouds, newCloud]);
        };

        const spawnIntervalId = setInterval(spawnCloud, 8000);

        return () => {
            clearInterval(spawnIntervalId);
        };
    }, [cloudImgs]);

    useEffect(() => {
        const resizeCanvas = () => {
            if (canvasRef.current) {
                canvasRef.current.width = window.innerWidth;
                canvasRef.current.height = window.innerHeight;
            }
        };

        window.addEventListener('resize', resizeCanvas);
        resizeCanvas();

        return () => window.removeEventListener('resize', resizeCanvas);
    }, []);

    return (
        <Box
            sx={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                backgroundColor: 'lightblue',
                overflow: 'hidden',
            }}
        >
            <canvas ref={canvasRef} onClick={handleCanvasClick} />
        </Box>
    );
};

export default Background;