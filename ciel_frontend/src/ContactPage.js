import React from 'react';
import { Typography, Container, Box, TextField, Button, Link } from '@mui/material';
import { Facebook, Twitter, Instagram } from '@mui/icons-material';

const ContactPage = () => {
    return (
        <Container maxWidth="sm">
            <Box sx={{ pointerEvents: 'auto', my: 4, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <Typography variant="h4" component="h1" gutterBottom sx={{ textAlign: 'center' }}>
                    Contact Us
                </Typography>
                <TextField
                    label="Name"
                    variant="outlined"
                    fullWidth
                    margin="normal"
                />
                <TextField
                    label="Email"
                    variant="outlined"
                    fullWidth
                    margin="normal"
                />
                <TextField
                    label="Message"
                    variant="outlined"
                    multiline
                    rows={4}
                    fullWidth
                    margin="normal"
                />
                <Button variant="contained" sx={{ mt: 2, backgroundColor: 'lightblue', color: 'black' }}>
                    Submit
                </Button>
                <Box sx={{ mt: 4, display: 'flex', justifyContent: 'center', gap: 2 }}>
                    <Link href="#" color="inherit">
                        <Facebook />
                    </Link>
                    <Link href="#" color="inherit">
                        <Twitter />
                    </Link>
                    <Link href="#" color="inherit">
                        <Instagram />
                    </Link>
                </Box>
                <Typography variant="body1" color="text.secondary" sx={{ mt: 2, textAlign: 'center' }}>
                    Email: info@cielbakery.com
                </Typography>
            </Box>
        </Container>
    );
};

export default ContactPage;