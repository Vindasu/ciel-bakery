import React from 'react';
import { Typography, Container, Box, Button } from '@mui/material';
import logo from './ciel_logo.png'; // adjust the path if necessary

function HomePage() {
    return (
        <Container maxWidth="sm" sx={{ position: 'relative', zIndex: 2 }}>
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: '69vh',
                    textAlign: 'center',
                    pointerEvents: 'none', // make this Box and its children click-through
                }}
            >
                <img src={logo} alt="Ciel logo" style={{ width: '100%', maxWidth: '250px' }} />
                <Typography variant="h4" component="h1" gutterBottom>
                    welcome to ciel bakery
                </Typography>
                <Typography variant="h5" component="h2">
                    the sky is only the beginning
                </Typography>
            </Box>
        </Container>
    );
}

export default HomePage;