import React, { useState, useEffect } from 'react';
import { Container, Box, Typography, Grid, Card, CardContent, CardMedia } from '@mui/material';
import axios from 'axios';

function StorePage() {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetchProducts();
    }, []);

    const fetchProducts = async () => {
        try {
            const response = await axios.get('http://localhost:8000/api/products/'); // Replace with your Django backend URL
            setProducts(response.data);
        } catch (error) {
            console.error('Failed to fetch products:', error);
        }
    };

    return (
        <Container maxWidth="md">
            <Box
                sx={{
                    pointerEvents: 'auto',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    minHeight: '100vh',
                    textAlign: 'center',
                    pt: 3,
                }}
            >
                <Typography variant="h4" component="h1" gutterBottom>
                    Store
                </Typography>
                <Grid container spacing={3} justifyContent="center">
                    {products.map((product) => (
                        <Grid item xs={12} sm={6} md={4} key={product.id}>
                            <Card sx={{ maxWidth: 265 }}>
                                <CardMedia
                                    component="img"
                                    height="140"
                                    image={product.image} // Adjust the path as needed
                                    alt={product.name}
                                />
                                <CardContent>
                                    <Typography variant="h5" component="div">
                                        {product.name}
                                    </Typography>
                                    <Typography variant="body2" color="text.secondary">
                                        {product.description}
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                    ))}
                    {/* Add some blank cards */}
                    {[...Array(10)].map((_, index) => (
                        <Grid item xs={12} sm={6} md={4} key={index}>
                            <Card sx={{ maxWidth: 265 }}>
                                <CardMedia
                                    component="img"
                                    height="140"
                                    image="" // Replace with the path to a placeholder image
                                    alt="Placeholder"
                                />
                                <CardContent>
                                    <Typography variant="h5" component="div">
                                        Placeholder
                                    </Typography>
                                    <Typography variant="body2" color="text.secondary">
                                        This is a placeholder.
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Box>
        </Container>
    );
}

export default StorePage;