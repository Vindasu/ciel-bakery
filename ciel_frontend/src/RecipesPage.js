// ContactPage.js
import React from 'react';
import { Typography, Box } from '@mui/material';

const RecipesPage = () => {
    return (
        <Box sx={{
            pointerEvents: 'auto',
            display: 'flex',
            justifyContent: 'center',
            my: 4
        }}>
            <Typography variant="h4" component="h1" gutterBottom>
                Recipes
            </Typography>
        </Box>
    );
};

export default RecipesPage;