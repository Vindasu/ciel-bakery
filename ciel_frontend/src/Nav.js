// Nav.js
import React from 'react';
import { AppBar, Toolbar, IconButton, Typography, Button, Box, Switch } from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';
import HomeIcon from '@mui/icons-material/Home';

const Nav = ({ darkMode, handleThemeChange }) => {
    return (
        <AppBar position="static" sx={{ background: 'transparent', boxShadow: 'none' }}>
            <Toolbar>
                <IconButton edge="start" color="inherit" aria-label="home" component={RouterLink} to="/">
                    <HomeIcon />
                </IconButton>
                <Box style={{ flexGrow: 1 }}>
                    <Typography variant="h6" style={{ textDecoration: 'none', color: 'inherit' }} component={RouterLink} to="/">
                        Ciel
                    </Typography>
                </Box>
                <Button color="inherit" component={RouterLink} to="/about">About</Button>
                <Button color="inherit" component={RouterLink} to="/store">Store</Button>
                <Button color="inherit" component={RouterLink} to="/recipes">Recipes</Button>
                <Button color="inherit" component={RouterLink} to="/contact">Contact</Button>
                <Box sx={{ display: 'flex', justifyContent: 'flex-end', p: 1 }}>
                    <Switch checked={darkMode} onChange={handleThemeChange} />
                </Box>
            </Toolbar>
        </AppBar>
    );
};

export default Nav;