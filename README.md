# Ciel Bakery


## Introduction

This is a Django/React Web Application designed to host the main website for Ciel Bakery, a new French/Japanese themed bakery cafe that will have merch, recipes, and contact information to begin. 

## Tech Stack

**Client:** React, Redux, Material-UI

**Server:** Node, Express, Django

**Database:** PostgreSQL, MongoDB


## Run Locally (on Docker Desktop)

### Clone the project

```
git clone https://gitlab.com/Vindasu/ciel-bakery

cd ciel-bakery

docker-compose up --build

Frontend on localhost:3000, backend on port 8000
```
